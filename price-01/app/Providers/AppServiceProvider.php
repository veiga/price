<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use App\Repositories\ProductRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\IProductRepository;
use App\Repositories\CachedProductRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        View::composer('nav', 'App\\Composer\\NavComposer');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(IProductRepository::class, function() {
            return new CachedProductRepository(app(ProductRepository::class));
        });
    }
}
