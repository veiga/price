<?php

namespace App;

class Product {
    public function __construct($product) {
        $this->unformattedDate = $product['date'];
        $this->unformattedPrice = $product['price'];
        $this->expiry_date = $product['expiry_date'];
        $this->bulk = $product['bulk'];
    }

    public function formattedExpiry() {
        if ($this->currentTime() - $this->expiry_date > 86400) {

        }
    }

    public function currentTime() {
        return now();
    }

    public function formattedDate() {
        return date('Y/n/j', $this->unformattedDate);
    }

    public function formattedPrice() {
        return sprintf('HKD%s', number_format($this->unformattedPrice, 2));
    }

    public function setBulk($bulk) {
        $this->bulk = $bulk;
    }

    public function getDiscountOfQuantity($quantity) {
        $sortedBulk = array_sort($this->bulk, function ($a, $b) {
            if ($a['quantity'] > $b['quantity']) {
                return -1;
            } elseif ($a['quantity'] < $b['quantity']) {
                return 1;
            } else {
                return 0;
            }
        });

        foreach ($sortedBulk as $bulk) {
            if ($quantity >= $bulk['quantity']) {
                return $bulk['discount'];
            }
        }

        return 0;
    }
}
