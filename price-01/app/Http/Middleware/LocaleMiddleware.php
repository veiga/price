<?php

namespace App\Http\Middleware;

use Closure;
use \Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class LocaleMiddleware
{
    public function __construct(Router $router) {
        $this->router = $router;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Below two have the same meanings
        //   $this->router->input('locale');
        //   $request->route('locale');

        App::setLocale($request->route('locale'));

        URL::defaults(['locale' => App::getLocale()]);

        return $next($request);
    }
}
