<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\DB;

class UserRepository {
    public function createUser($username, $password) {
        return DB::table('users')->insertGetId([
            'username' => $username,
            'password' => $password
        ]);
    }

    public function map($user) {
        // return array_map(function($user) {
            return new User($user);
        // }, $users);
    }

    public function mapAll($users) {
        $result = [];
        foreach ($users as $user) {
            array_push($result, new User($user));
        }
        return $result;
    }

    public function getAll() {
        return $this->mapAll(DB::table('users')->get());
    }

    public function getUserByUsername($username) {
        return $this->map(DB::table('users')->where(
            'username', $username
        )->first());
    }

    public function getUserByFacebookId($facebookId) {
        return $this->map(DB::table('users')->where(
            'facebook_id', $facebookId
        )->first());
    }

    public function linkUserWithFacebookId($userId, $facebookId) {
        return DB::table('users')->where(
            'id', $userId
        )->update([
            'facebook_id' => strval($facebookId)
        ]);
    }
}
