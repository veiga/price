<?php

namespace App\Console\Commands;

use App\Mail\GreetingMail;
use Illuminate\Console\Command;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'price:send-emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = $this->userRepository->getAll();
        foreach ($users as $user) {
            Mail::to($user)->send(new GreetingMail());
        }
    }
}
