<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColorToCar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car', function (Blueprint $table) {
            // add color column
            $table->string('color')->default('');

            // change price column to original price
            $table->renameColumn('price', 'original_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car', function (Blueprint $table) {
            // drop color column
            $table->dropColumn('color');

            // change original price column to price
            $table->renameColumn('original_price', 'price');
        });
    }
}
