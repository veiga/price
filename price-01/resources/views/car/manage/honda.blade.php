@extends('car.layouts')

@section('title')
{{ __('Car Management') }}
@endsection

@section('content')

<h1>HONDA PAGE</h1>

@forelse($cars as $car)

<div>
    <div class="name">{{ $car->name }}</div>
    <div class="color">{{ $car->color }}</div>
    <div class="price">{{ $car->formattedPrice() }}</div>
</div>

@empty

No Cars.

@endforelse

<form action="{{ route('car.manage.addHonda') }}" method="POST">
@csrf
Color: <input type="text" name="color" /><br>
Price: <input type="text" name="price" /><br>
<input type="submit" />
</form>

@endsection
