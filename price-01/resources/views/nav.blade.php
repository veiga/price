<ul>
    <li>{{ __('You are at:') }} {{ $slot }}</li>
    <li>{{__('Home')}}</li>
    <li>{{__('Profile')}}</li>
    <li>{{__('Car')}}</li>
    <li>{{ __('Latest Car Submitted:')}} {{ $latestCar->name }}</li>
</ul>
