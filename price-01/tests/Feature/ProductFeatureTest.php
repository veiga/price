<?php

namespace Tests\Feature;

use App\Product;
use Tests\TestCase;
use App\Repositories\ProductRepository;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductFeatureTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() {
        parent::setUp();
        $this->repository = new ProductRepository();
    }

    private function newProduct() {
        $product = new Product([
            'date' => mktime(0, 0, 0, 1, 1, 2018),
            'price' => 1000,
            'expiry_date' => null,
            'bulk' => []
        ]);
        $this->repository->addProduct($product);
    }

    // public function testNothing() {
    //     $this->assertDatabaseHas('asdfadfasdf', [
    //         'price' => 1000,
    //         'date' => mktime(0, 0, 0, 1, 1, 2018)
    //     ]);
    // }

    public function testCreate() {
        $this->newProduct();
        $this->assertDatabaseHas('products', [
            'price' => 1000,
            'date' => date('Y-m-d', mktime(0, 0, 0, 1, 1, 2018))
        ]);
    }

    public function testList() {
        $products = $this->repository->getAll();
        $this->assertEquals(0, count($products));
    }

    public function testList2() {
        $this->newProduct();
        $products = $this->repository->getAll();
        $this->assertEquals(1, count($products));
    }

    public function testList3() {
        $this->newProduct();
        $this->newProduct();
        $this->newProduct();
        $products = $this->repository->getAll();
        $this->assertEquals(3, count($products));
    }

    public function testRemoveExpired() {
        $this->newProduct();
        $this->newProduct();
        $this->newProduct();

        $this->repository = \Mockery::mock($this->repository, [
            'getCurrentTime' => mktime(0, 0, 0, 1, 1, 2019)
        ]);

        $product = new Product([
            'date' => mktime(0, 0, 0, 1, 1, 2018),
            'price' => 2000,
            'expiry_date' => mktime(0, 0, 0, 1, 1, 2018),
            'bulk' => []
        ]);
        $this->repository->addProduct($product);

        // Before removal, all products are here
        $products = $this->repository->getAll();
        $this->assertEquals(4, count($products));

        // Remove the last product with expiry
        $this->repository->removeAllExpired();

        // Only 3 remaining products are without expiry
        $products = $this->repository->getAll();
        $this->assertEquals(3, count($products));

        $this->assertDatabaseMissing('products', [
            'price' => 2000
        ]);
    }
}
