<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HashTest extends TestCase
{
    public function testExample()
    {
        $hashed = Hash::make('test');

        $this->assertEquals(true, Hash::check('test', $hashed));
    }

}
